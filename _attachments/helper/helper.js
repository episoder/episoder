// helper javascript for episodes app
//
//
//
//
//
Date.prototype.toAirdateFormat = function() {
    return this.getUTCFullYear()   + '-' +
           f(this.getUTCMonth() + 1) + '-' +
           f(this.getUTCDate());  
};

Date.fromAirdate = function(input)
{
  var d = new Date();
  var year = input.substring(0,4);
  var month = input.substring(5,7);
  var day = input.substring(8,10);
  d.setFullYear(year,month -1,day);
  return d;
};
/**
 * returns the difference in days
 */
Date.diffFromAirdate = function(input)
{
  var airdate = Date.fromAirdate(input);
  var diff = new Date(airdate - new Date());
  return Math.floor(diff/1000/24/60/60);
};

function f(n) {    // Format integers to have at least two digits.
    return n < 10 ? '0' + n : n;
}
function arrunique(arr) 
{
    var xxa = [];
    var xxl = arr.length;
    for(var xxi=0; xxi<xxl; xxi++) 
    {
        for(var xxj=xxi+1; xxj<xxl; xxj++) 
        {
            // If arr[xxi] is found later in the array
            if (arr[xxi] === arr[xxj])
            {
                xxj = ++xxi;
            }
        }
        xxa.push(arr[xxi]);
    }
    return xxa;

}


/*
 * lists all shows
 */
function showShows()
{    
   var url =  "/episodes/_design/episodes/_view/listShows?group=true";
       jQuery.getJSON(url,{}, function(data)
        {

            $("h1").append("(" + data.rows.length +")" );

            var firstChar = new Array();
            for( var ep in data.rows )
            {
                var name = data.rows[ep].key.show;
                var showname = data.rows[ep].key.show;
                var showid = data.rows[ep].key.showid;
                var first= name.substring(0,1);
                firstChar[ep] = first;
                var html = "<li name=\""+name+"\" ";
                html += "style=\"display:block\">";
                html += "<a id=\""+showid+"\" class=\""+first+"\" " +
                    " href=\"#\" onclick=\"listShow('";
                html += name.replace(/'/g,"\\'");;
                html += "')\">";
                html += "<div>";
                html += name;
                html += "</div>";
                html += "</a></li>";
                $("#data").find("ul").append(html);
                $("#"+showid).hover(
                        function(){
                           var markerspan = "<span id=\""+this.id+"marker\" class=\"marker\" >";
                           markerspan += "<div>";
                           markerspan += "<img src=\"pictures/haken.png\" />";
                           markerspan += "</div>";
                           markerspan += "</span>";
                           $(this).before(markerspan);

                          var deletespan = "<span id=\""+this.id+"delete\" class=\"delete\">";
                          deletespan += "<div>"
                          deletespan += "<img src=\"pictures/delete.png\" />";
                          deletespan += "</div>"
                          deletespan += "</span>"
                          $(this).before(deletespan);
                            
                          // add delete function
                          $("#"+this.id+"delete").click(function(){
                                var show_id = this.id.replace("delete","");
                                var url = "/episodes/_design/episodes/_view/listShowId?";
                                url += "key=\"" + show_id + "\"";
                                jQuery.getJSON(url,{}, function(data){ 
                                 // get documents
                                 var docArray = new Array();
                                 for(idx in data.rows)
                                  {
                                       var doc = data.rows[idx].value;
                                       doc._deleted = true;
                                       docArray.push(doc);
                                  }
                                  var bulkurl = '/episodes/_bulk_docs';
                                  jQuery.ajax({
                                      url: bulkurl, type: "POST", dataType: "json",
                                      contentType: "application/json",
                                      data: jQuery.jqCouch.toJSON({docs: docArray}),
                                      error: function(req){alert("could NOT delete documents");},
                                      success: function(req){
                                        alert("could delete documents");
                                        var durl = '/episodes/' + show_id;
                                          jQuery.getJSON(url,{}, function(data){ 
                                                data._deleted = true;
                                                jQuery.ajax({
                                                  url: durl, type: "POST", dataType: "json",
                                                  contentType: "application/json",
                                                  data: jQuery.jqCouch.toJSON({docs: data}),
                                                  error: function(req){alert("could NOT delete main documents");},
                                                  success: function(req){alert("could delete main documents");},
                                                });
                                          });
                                      },
                                  });
                                });
                           });

                            // add mark function
                            $("#"+this.id+"marker").click(function(){
                                        // todo add bulk update here
                                      // alert("here");
                                   var _showid = this.id.replace("marker","");
                                   var key = _showid;
                                   var url = "/episodes/_design/episodes/_view/listShowId?";
                                   url += "key=\"" + key + "\"";
                                   jQuery.getJSON(url,{}, function(data){
                                   // get documents
                                   var docArray = new Array();
                                   for( idx in data.rows )
                                    {
                                        var doc =  data.rows[idx].value;
                                        doc.seen = "true";
                                        docArray.push(doc);
                                    }
                                    var bulkurl = '/episodes/_bulk_docs';
                                    jQuery.ajax({
                                    url: bulkurl,
                                    type: "POST",
                                    dataType: "json",
                                    contentType: 'application/json',
                                    data: jQuery.jqCouch.toJSON({ docs: docArray }),
                                    error: function(req){alert('error during update');},
                                    success: function(req){alert('show has been marked as \'seen\'');},
                                    });
                                   });
                                });

                        },
                        function(){
                          $(this).parent("li").find("span").remove();
                        } 
                );

                
            }
                       // make char clickable list
            var html = "<a class=\"charlink\" href=\"#\">";
            html+= "Actual";
            html+= "</a>  ";
            html+= "<a class=\"charlink\" href=\"#\">";
            html+= "All";
            html+= "</a>  ";
            html+= "<a class=\"charlink\" href=\"#\">";
            html+= "Upcoming";
            html+= "</a>  ";
            $("#charindex").append(html);
            var startChars = arrunique(firstChar);
            $.each(startChars, function(idx,value)
                    {
                        var html = "<a class=\"charlink\" href=\"#\">";
                        html+= value
                        html+= "</a>  ";
                        $("#charindex").append(html);
                    });
                    $("#charindex").find("a").click(function(event)
                    {
                        var selectedChar = event.target.childNodes[0].wholeText;
                        if(selectedChar=='All')
                        {
                            $(this).parent("li").hide(200);
                            $("#data a").parent("li").show(500);
                        }
                        else if(selectedChar=='Actual')
                        {
                           showActual();
                        }
                        else if(selectedChar=='Upcoming')
                        {
                           showUpcoming();
                        }
                        else
                        {
                            $("#data a[class!='"+selectedChar+"']").parent("li").hide(200);
                            $("#data a[class='"+selectedChar+"']").parent("li").show(500);
                        }
                    });
        });
        $("#pastlink").click(function(event){
        event.preventDefault();
        showActual();
        });
        $("#futurelink").click(function(event){
        event.preventDefault();
        showUpcoming();
        });
}

function showActual()
{
   var today = new Date().toAirdateFormat();
   var startkey = today;
   var endkey = "";
   var url = "/episodes/_design/episodes/_view/latest?startkey=\""+startkey+
             "\"&endkey=\""+endkey+"\"&descending=true&limit=20";
   hidePage();
    jQuery.getJSON(url,{}, function(data){
       $("#data").empty();
       for(var idx in data.rows)
        {
          addEpData(data.rows[idx].value);
          // var diff
          var airdate = data.rows[idx].value.airdate;
          var diffdays = Date.diffFromAirdate(airdate);
          $("#"+data.rows[idx].value._id+"hovertarget").prepend("("+diffdays+"d)");
        }
        $("#data").show(500);
        $("h1").empty;
        $("h1").html("Past Episodes");
        showBacklink();
        });
}
function showBacklink()
{
        $("#back").empty();
        $("#back").html("back");
        $("#back").click(function(event){
                window.location.reload();
        });

}

function showUpcoming()
{
   var today = new Date().toAirdateFormat();
   var startkey = today;
   var url = "/episodes/_design/episodes/_view/latest?startkey=\""+startkey+
             "\"&limit=20";
   hidePage();
   jQuery.getJSON(url,{}, function(data){
       $("#data").empty();
       for(var idx in data.rows)
        {
          addEpData(data.rows[idx].value);
          // var diff
          var airdate = data.rows[idx].value.airdate;
          var diffdays = Date.diffFromAirdate(airdate);
          if(diffdays==0) 
          {
            $("#"+data.rows[idx].value._id+"hovertarget").css("color","red");
          }
          $("#"+data.rows[idx].value._id+"hovertarget").prepend("("+diffdays+"d)");
        }
        $("#data").show(500);
        $("h1").empty;
        $("h1").html("Upcoming Episodes");
        showBacklink();
        });

}

function toggleShow(key,descending)
{
   var startkey = "[\""+key+"\"]";
   var endkey = "[\""+key+"\",{}]";
   var url_postfix = "";
    if(eval(descending))
    {
        var tmp = startkey;
        startkey=endkey;
        endkey=tmp;
        url_postfix = "&descending="+ descending;
        descending=false;
    }
    else { descending=true };
   
  var url = "/episodes/_design/episodes/_view/listShow?startkey=" + startkey + "&endkey=" + endkey;
    url += url_postfix;
    $("#data").hide(200);
    jQuery.getJSON(url,{}, function(data)
    {
        var showid = data.rows[0].value.showid; 
        $("#data").empty();
        for(var idx in data.rows)
        {
            addEpData(data.rows[idx].value);
        }
    })
    $("#sortep").remove();
    $("#data").before("<a id=\"sortep\" href=\"#\" onclick=\"toggleShow('"+(key.replace(/'/g, "\\'"))+ "', '"+descending+"')\">flip sorting</a>");;
    $("#data").show(500)
}
/*
 * lists a single show and its episodes with given key 
 * and argument for descending or not.
 */
function listShow(key,descending)
{
    var startkey = "[\""+key+"\"]";
    var endkey = "[\""+key+"\",{}]";
    var url_postfix = "";
    if(descending!=null)
    {
        var tmp = startkey;
        startkey=endkey;
        endkey=tmp;
        url_postfix = "&descending=true";
    }

    var url = "/episodes/_design/episodes/_view/listShow?startkey=" + startkey + "&endkey=" + endkey;
    url += url_postfix;
    hidePage();
    $("h1").hide("slow", function(){
            $("h1").html(key);
            $("h1").show("fast");
            });
    $("#data").hide("slow", function(){
            $("#data").find("ul").html("");
            jQuery.getJSON(url,{"ey":"data"}, function(data)
                {
                var showid = data.rows[0].value.showid; 
                showPic(showid);
                $("h1").after("<div class=\"epcounter\"> [#eps:"+data.rows.length+"]</div>");
                for(var idx in data.rows)
                {
                    addEpData(data.rows[idx].value);
                }
                $("#data").show("slow");
                $("#sortep").remove();
                $("#data").before("<a id=\"sortep\" href=\"#\" onclick=\"toggleShow('"+(key.replace(/'/g, "\\'"))+ "', "+true+")\">flip sorting</a>");
                $("h1").before("<a href=\"/episodes/_design/episodes/index.html\">all shows</a>");

                });
    });
}

/*
 * inserts the picture for the show
 */
function showPic(showkey)
{
    $("#eppic").hide(200,function()
            {
            $("#eppic").empty();
            var src = "/episodes/" + showkey + "/cast.jpg";
            var html = "<img src=\"" + src + "\">&nbsp;</img>";
            $("#eppic").append(html);
            $("#eppic").show(500);
            });
}

function addEpData(epdata)
{
    // episode info itself
    var inner = "<div id=\""+epdata._id+"hovertarget\" class=\"hovertarget\">";
    inner += epdata.show.bold();
    inner+=" s"+epdata.season+"e"+epdata.episode+": "+ epdata.airdate+" :"+epdata.title+"</div>";
    // wrap it up
    var html = "<div id=\""+epdata._id+"\" class=\"episodewrapper\">";
    html += inner;
    html += "</div>";
    $("#data").append(html);

    if(epdata.seen && epdata.seen == "true")
    {
     $("#"+epdata._id).addClass("seen");
    }

    // adding iso hunt clickable link
    $("div[id='"+epdata._id + "hovertarget']").hover(
       function()
       {
           var isospan = "<span class=\"isohunt\">";
           isospan += "<a target=\"_blank\" href=\"";
           isospan += "http://isohunt.com/torrents.php?ihq=";
           // remove air date
           // s3e19: 2010-04-01 :Eyes Wide Open
           isospan += epdata.show
           isospan += "+"
           isospan += epdata.episode
           isospan += "&ext=&op=and";
           isospan += "\" >";
           isospan += "<img src=\"pictures/isohunt.png\" />";
           isospan += "</a>";
           isospan += "</span>";
           $(this).append(isospan);

           var markerspan = "<span class=\"marker\" >";
           markerspan += "<img src=\"pictures/haken.png\" />";
           markerspan += "</span>";
           $(this).append(markerspan);
           $("#"+epdata._id+" .marker").click(function(){
                
            if(epdata.seen && epdata.seen == "true")
                {
                 epdata.seen = "false";
                 $("#"+epdata._id).removeClass("seen");
                }
                else
                {
                 epdata.seen = "true";
                 $("#"+epdata._id).addClass("seen");
                }
                 saveDocument(epdata);
                });


       },
       function()
       {
           $(this).find("span").remove();
       }
    );

}

function saveDocument(doc)
{
  var url = "/episodes/" + doc._id + " -d " + escape(uneval(doc)); 
  var result = $.jqCouch.connection('doc').save('episodes',doc);
}

function hidePage()
{
    $("#charindex").hide("slow");
    $("#search").hide("slow");
    $("#data").fadeOut("fast");
    // animate retro and future links
    var upcomingCss = 
        { 
          'left': '-=300px',
          'top' : '-=130px',
        };
    var laterCss = 
        { 
          'left': '-=50px',
          'top' : '-=300px',
        };
    if($("#pastlink").css('top') == '300px')
    {
        $("#pastlink").animate(laterCss,800);
        $("#futurelink").animate(upcomingCss,800);
    }
}
